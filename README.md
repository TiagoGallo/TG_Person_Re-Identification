# Person re-identification

## Trabalho de Graduação de engenharia mecatrônica da UnB

Aluno: Tiago de Carvalho Gallo Pereira (14/0164138)

Orientador: Teófilo Emídio de Campos (01094092)

### Resumo da proposta

No universo da visão computacional, a re-identificação de pessoas em diferentes vistas é um desafio ainda não dominado. Cada vista pode apresentar diferentes características nas imagens das pessoas, seja angulação, iluminação, foco, distorções, roupas. Podemos denominar esse conjunto de variáveis como um domínio. A re-identificação de pessoas em diferentes domínios é problema de grande complexidade.

Uma solução clássica para re-identificar pessoas em diferentes vistas seria utilizar biometria facial, no entanto há várias restrições para o funcionamento dessa solução. Por exemplo, a câmera precisa estar focada no rosto da pessoa (frontal e na altura do rosto), há dependência da densidade de pixels no rosto da pessoa.

O objetivo do presente trabalho é estudar uma solução para o desafio de re-identificação de pessoas em diferentes domínios, que utilize todo o escopo de dados disponível na imagem da pessoa. Para isso, analisaremos as técnicas existentes para re-identificação de pessoas, validaremos essas técnicas em bases de dados públicas e proporemos uma nova técnica, ou combinação de técnicas existentes.

### Relação com a mecatrônica

No campo da robótica, relacionado ao atendimento de pessoas, existe um desafio que é saber se já houve interação entre uma pessoa e o robô.

Exemplos de aplicações:

* **Garçom** - Um robô garçom que atende várias pessoas ao mesmo tempo, ao reencontrar uma pessoa, precisa recuperar o contexto da interação;
* **Hospital** - Um conjunto de robôs de atendimento em um hospital necessita compartilhar informações sobre os pacientes atendidos em diferentes momentos e interações.

Outro campo muito forte da mecatrônica é o campo de automação que também pode se beneficiar da re-identificação de pessoas.  Em todos os processos de automação em que se controla o tempo e o movimento de pessoas, a re-identificação de pessoas por visão computacional é uma solução que afeta a relação qualidade x custo do processo. Ela substitui, por exemplo, o controle por RFID.  

Exemplos de aplicações:

* **Chão de fábrica** - A re-identificação de pessoas permite uma medição de tempo e movimento, relacionado a cada processo, das pessoas em ambiente fabril;
* **Parque temático** - A automação utilizando visão computacional e re-identificação de pessoas permite medir tempo médio de fila para cada atração, preferência das atrações, quantidade média de atrações que uma pessoa consegue visitar, preferência das pessoas para cada atração por meio da identificação da ordem de escolha das pessoas;
* **Segurança** - Em ambientes controlados, a re-identificação de pessoas possibilita controlar o fluxo de pessoas em áreas restritas e alertar a segurança quando identificar uma pessoa não autorizada.

#### Link para pasta com os PDFs dos artigos

https://drive.google.com/open?id=1gndBthwOCxk1DO2FON78yVNWl2TXh8sH

### Repositórios do GitHub com códigos que podem ser utéis

* **SpindleNet** - https://github.com/yokattame/SpindleNet
* **Open-ReID**  - https://github.com/Cysu/open-reid