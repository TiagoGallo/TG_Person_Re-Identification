
\chapter{Introdução}

\label{CapIntro}

% Resumo opcional. Comentar se não usar.
\resumodocapitulo{}

A re-identificação de pessoas é um grande desafio no campo da visão computacional. Para 
entender melhor a complexidade e a necessidade de se resolver esse desafio, este capítulo
apresenta uma motivação do tema na seção \ref{contex}. O escopo do problema é analisado 
na seção \ref{definition} e os objetivos propostos são apresentados na seção \ref{objetivos}. 
A seção \ref{conclusao} resume os resultados obtidos.

\section{Motivação}
\label{contex}

Atualmente, as pessoas necessitam cada vez mais de se sentirem seguras e protegidas. Os \textit{hadrwares}, 
como câmeras de segurança e gravadores de vídeos digital, se tornaram equipamentos com preços acessíveis ao público
geral. Com a combinação desses dois fatores, houve um crescimento muito grande na quantidade de CFTV \footnote{
Os circuitos fechados de televisão são sistemas internos de monitoramento e vigilância, esses sistemas são 
compostos por câmeras de segurança e gravadores de vídeos digital, que são responsáveis por apresentar as 
imagens das câmeras de segurança em monitores e, se necessário, gravar as imagens dessas câmeras.}
(circuito fechado de televisão) no mundo. Portanto, há uma grande massa de dados de vídeos 
e imagens sendo geradas o tempo inteiro.

No entanto, o custo para se analisar todos esses dados é muito alto. Por exemplo, para um grande
estabelecimento, como um \textit{shopping}, seria necessária uma equipe de funcionários responsáveis por 
monitorar as imagens e extrair informações úteis, em tempo integral. A Figura \ref{fig:monitoramento} mostra 
um grande centro de monitoração e operação na tecnologia atual.

\begin{figure}[htbp]
    \centering

    {\includegraphics[width=1.0\textwidth]{figs/monitoramento.jpg}}
    
    \caption{Ilustração típica de equipes de monitoramento responsável por 
    CFTV de grandes estabelecimentos. Reproduzida de \cite{cctv}.}
    \label{fig:monitoramento}
\end{figure}

Portanto, há um problema de necessidades vs custo de operação para tornar viável a interpretação
dos dados dos CFTV. Atrelado a esse problema, há um custo de oportunidade entre a quantidade de câmeras instaladas
e o custo das pessoas para analisá-las. No entanto, os estudos no campo da visão computacional tem 
sido desenvolvidos com o intuito de facilitar e tornar menos entediante esse trabalho.

Para cada conjunto de informações que pode-se obter com o monitoramento de câmeras de um CFTV, existem 
técnicas de visão computacional sendo desenvolvidas para resolver esses problemas, com redução na necessidade de 
intervenção humana. Por exemplo, técnicas de reconhecimento de ações estão sendo desenvolvidas para 
detectar atividades suspeitas \cite{surveillance}, técnicas de reconhecimento facial são desenvolvidas para autenticar pessoas \cite{DeepFace},
técnicas de re-identificação de pessoas estão sendo desenvolvidas para acompanhar a movimentação de uma 
pessoa, dentro de um ambiente onde ela é vista por várias câmeras diferentes. 

\section{Definição do problema}
\label{definition}

A re-identificação é o processo de identificar uma pessoa a partir de várias imagens ou vídeos capturados
de diferentes câmeras, quando não há sobreposição entre as vistas de cada câmera, ou de uma mesma
câmera em momentos distintos. Ou seja, dado um conjunto de dados (imagens ou vídeos), o problema consiste
em associar esses dados a uma pessoa e conseguir identificar essa pessoa em diferentes câmeras e diferentes
momentos no tempo. 

Explicando o problema de um ponto de vista prático, pode-se pensar no desafio de acompanhar uma pessoa
que está passeando por um \textit{shopping center}, sem precisar
que alguém fique olhando para as câmeras e procurando essa pessoa. Portanto, o algoritmo utiliza
das imagens das câmeras de segurança do \textit{shopping center} e aprende a aparência da pessoa (pode-se dizer que
o algoritmo aprende uma assinatura da pessoa). Logo, o desafio é criar esse algoritmo que saiba que 
a pessoa que entrou pela garagem é a mesma que passou em frente ao cinema e também é a mesma que está
sentada na praça de alimentação.

Cada câmera tem várias caraterísticas diferentes, como angulação, iluminação, distorções. Além disso, 
as pessoas podem apresentar variações quando vistas em câmeras diferentes, como roupas (a pessoa
pode colocar/retirar um casaco ou chapéu) ou posição do corpo (a pessoa pode ser vista de costas em uma
câmera e de frente ou de lado na outra). O conjunto de variáveis são denominados um domínio, ou seja
pode-se tratar cada câmera como um domínio distinto. O desafio passa a ser identificar as pessoas em
diferentes domínios, esse fato acrescenta uma grande complexidade ao problema. A Figura
\ref{fig:exemplo} mostra o quanto a aparência de uma pessoa pode mudar de uma câmera para outra, 
dificultando o processo de re-identificação.

\begin{figure}[htbp]
    \centering

    \subfigure
        {\includegraphics[width=0.15\textwidth]{figs/pessoa9_cam0.jpg}}
    \qquad
    \subfigure
        {\includegraphics[width=0.15\textwidth]{figs/pessoa9_cam1.jpg}}%
    
    \caption{Exemplo de imagens de uma mesma pessoa em diferentes câmeras.
     Fonte: Base de dados CUHK03 \cite{deepReID}. \copyright 2014 IEEE.}
    \label{fig:exemplo}
\end{figure}

\section{Objetivos do projeto}
\label{objetivos}

O objetivo do presente trabalho é estudar uma solução para o desafio de re-identificação de 
pessoas em diferentes domínios, utilizando todo o escopo de dados disponível na imagem da pessoa.
Inicialmente, são analisadas algumas técnicas existentes para re-identificação de pessoas. Validadas essas
técnicas em bases de dados públicas, e proposta uma nova técnica, ou combinação de técnicas existentes.

Outro objetivo desse trabalho foi o de resolver, para a \textit{CyberLabs} (\textit{startup} brasileira), o problema 
de cronometrar o tempo de pessoas em uma fila, de forma automatizada. Para isso, é utilizado o algoritmo
desenvolvido de re-identificação de pessoas. As imagens da mesma pessoa são agrupadas na vista da 
entrada e na vista da saída da fila. O tempo entre esses dois grupos de imagens é cronometrado para 
estimar o tempo que essa pessoa passou na fila.

Objetiva-se, também, neste trabalho, estudar o potencial de alguns métodos de adaptação de domínio.
A ideia é possibilitar a utilização, em um domínio alvo, de redes neurais treinadas em um domínio fonte 
e obter resultados melhores que o aleatório, e melhores que redes neurais que não usaram os métodos 
de adaptação de domínio.

\section{Resultados obtidos}
\label{conclusao}

Obteve-se um bom conhecimento sobre as técnicas utilizadas e os atuais resultados
obtidos pelo estado da arte, no desafio de re-identificação de pessoas, a partir do estudo da literatura. 
Além do mais, foram treinadas redes neurais que obtiveram resultados razoáveis em bases de dados públicas 
que são utilizadas para \textit{benchmark}\footnote{O processo de \textit{benchmark} consiste em utilizar
sempre as mesmas condições de testes para avaliar algum algoritmo, os resultados são anotados no final do 
processo com o objetivo de serem utilizados como referência para futuros testes que desejam melhorar os 
resultados obtidos a priori.}. Os resultados obtidos ainda estão distantes do estado da arte, no entanto 
já se demonstram muito melhores do que um palpite aleatório e já tem uso prático.

Criou-se também uma base de dados proprietária (\textit{CyberQueue}) com o objetivo de resolver um problema para 
a \textit{CyberLabs}. Essa base de dados não pode ser publicada por questões de direitos autorais de 
imagem, mas foi utilizada para testes de adaptação de domínio e para transferência de conhecimento durante 
o treinamento das redes neurais. Ademais, treinou-se uma rede especialista para a base de dados \textit{CyberQueue},
essa rede alcançou resultados de treino satisfatórios e aplicabilidade prática com uma boa performance.

Os testes com métodos de adaptação de domínio mostraram todo o potencial desse tipo de técnica. Mesmo não 
alcançando resultados tão bons como os treinamentos em domínio específico, os métodos de adaptação de 
domínio mostraram-se melhores que o aleatório e foi possível ver um incremento no resultado a cada novo
método implementado. Portanto, acredita-se que esse tipo de técnica está em uma fase inicial de 
estudos e ainda tem capacidade para evoluir bastante, resolvendo a dificuldade de trabalhar com múltiplos 
domínios.