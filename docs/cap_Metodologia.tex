
\chapter{Metodologia}

% Resumo opcional. Comentar se não usar.
\resumodocapitulo{}

\label{CapMetodologia}

Esse capítulo visa explicar os métodos escolhidos para resolver o desafio de re-identificação
de pessoas e o porque esses métodos são interessantes. Na seção \ref{face_rec} é feita uma 
comparação entre os métodos de reconhecimento facial e de re-identificação de pessoas e é 
explicado o porque é necessário estudar o desafio de re-identificação. A seção \ref{params}
vai apresentar a rotina criada para o treinamento de redes que serão treinadas e avaliadas no 
mesmo domínio. A seção \ref{met_adap} mostra todos os métodos de adaptação de domínio que 
serão usados. Já a 
seção \ref{problema} apresenta um método proposto para resolver um problema comum no treinamento 
de redes neurais, quando se usa a função de custo \textit{triplet}. Por fim, a seção \ref{metricas}
apresenta todas as métricas que serão utilizadas para avaliar os resultados gerados.

\section{Falha do reconhecimento facial}
\label{face_rec}

Hoje, a tecnologia do reconhecimento facial se encontra muito mais desenvolvida que a tecnologia de 
re-identificação de pessoas. Pode-se afirmar isso dado que a rede neural \textit{DeepFace} \cite{DeepFace}
criada por Taigman et al.\ em 2014 (5 anos atrás) obteve uma performance praticamente igual a de um ser 
humano quando avaliada na base de dados LFW (\textit{Labeled Faces in the Wild}) \cite{LFW}. A base de dados
LFW contém 13233 imagens de 5749 pessoas distintas sendo todas essas imagens pertencentes a situações reais 
(vide Figura \ref{fig:lfw_ex}). A acurácia da rede \textit{DeepFace} nessa base de dados foi de $97.35\%$
enquanto a acurácia humana nessa base de dados é de $97.53\%$.

\begin{figure}[htbp]
    \centering

    {\includegraphics[width=1.0\textwidth]{figs/LFW.png}}
    
    \caption{Exemplos de imagens da base de dados LFW. Reproduzida de \cite{LFW}.}
    \label{fig:lfw_ex}
\end{figure}

Enquanto o estado da arte de reconhecimento facial está muito próximo ou até um pouco melhor do que a
capacidade humana nessa tarefa, o desafio de re-identificação de pessoas atinge taxas de erros
próximas a $8\%$ para as bases de dados \textit{CUHK03} e \textit{Market1501}. Então, porque não usar 
apenas o reconhecimento facial, ao invés de tentar desenvolver técnicas de re-identificação de pessoas?

A grande diferença entre esse dois desafios é que o reconhecimento facial necessita de imagens de alta 
qualidade (resolução) e que peguem bem o rosto das pessoas. Enquanto para a re-identificação de pessoas,
pode-se trabalhar com imagens de menor qualidade e sem restrição quanto a pose da pessoa, podendo, por 
exemplo, utilizar imagens de pessoas de costas. Como o foco do desafio de re-identificação de pessoas 
é utilizar imagens provenientes de CFTV (câmeras de segurança), as técnicas de reconhecimento facial 
não funcionam bem para esse tipo de dados.

Utilizando imagens da base de dados \textit{CUHK03} e a biblioteca de código aberto \textit{dlib} \cite{dlib}, 
pode-se visualizar o resultado dos algoritmos de reconhecimento facial nas imagens de CFTV. 
A Figura \ref{fig:falha_rec_facial} demonstra o caso onde a pose da pessoa não permite a visualização
do rosto (imagem mais a esquerda) e uma situação onde o rosto é visível, mas com baixa qualidade 
(imagem central). Na imagem mais a direita da Figura \ref{fig:falha_rec_facial}, pode-se ver que os pontos 
chaves do rosto (rosto da imagem do meio) que foram encontrados estão um pouco amontoados, mas corretos. 
No entanto, para conseguir detectar esses pontos foi necessário fazer uma interpolação na imagem deixando 
ela 4 vezes maior que seu tamanho original. Portanto, houve uma inserção de ruído desnecessário na imagem e 
um aumento no custo computacional do algoritmo.

\begin{figure}[htbp]
    \centering

    \subfigure
        {\includegraphics[width=0.15\textwidth]{figs/CUHK_no_face.jpg}}
    \qquad
    \subfigure
        {\includegraphics[width=0.15\textwidth]{figs/CUHK_face.png}}%
    \qquad
    \subfigure
        {\includegraphics[width=0.15\textwidth]{figs/CUHK_face_ex.png}}%
    
    \caption{Ilustração da dificuldade de usar reconhecimento facial nas bases de dados de re-identificação
    de pessoas. \textit{Esquerda:} Exemplo de imagem onde o rosto não é visível. \textit{Meio:} Exemplo de 
    imagem onde o rosto é visível, mas com uma baixa qualidade. \textit{Direita:} Pontos chaves encontrados 
    no rosto da imagem do meio, a imagem do rosto teve que ser interpolada para a detecção desses pontos, por 
    isso apresenta-se distorcida.}
    \label{fig:falha_rec_facial}
\end{figure}

As dificuldades apresentadas demonstram que mesmo o reconhecimento facial obtendo ótimos resultados e 
funcionando para uma gama de casos, há espaço para desenvolver o desafio de re-identificação de pessoas.
Pois, em diversos casos, o reconhecimento facial não será suficiente e a re-identificação de pessoas poderá
resolver o problema com maestria. Por exemplo, em casos onde deseja-se fazer rastreamento de
pessoas usando as imagens de CFTV.

\section{Treinamento das redes neurais de re-identificação de pessoas}
\label{params}

Como visto na seção anterior, há a necessidade de treinar redes neurais especialistas no desafio de re-identificação
de pessoas, portanto o primeiro passo desse trabalho será treinar essas redes. Será treinada uma rede neural para 
cada base de dados apresentada no capítulo \ref{CapBaseDeDados} de forma a analisar o potencial do aprendizado 
supervisionado, utilizando redes profundas nessas bases de dados.

Todas as redes treinadas utilizaram a arquitetura \textit{Resnet 50} que é uma arquitetura 
residual. Para a função de custo e o otimizador foram escolhidos a \textit{triplet} e o Adam, respectivamente.
Antes de fazer o treinamento, todas as imagens de todas as bases de dados foram convertidas para a 
resolução de $128\times256$ \textit{pixels}. Esse é um valor médio entre as resoluções das 
bases de dados utilizadas. A criação de uma resolução padrão facilita os testes e as avaliações em 
domínios distintos.

Nesses treinamentos, os pesos da rede neural foram inicializados utilizando os pesos de uma rede 
treinada na base de dados \textit{ImageNet} \cite{imagenet_cvpr09}. Portanto, foi utilizada a técnica
de \textit{fine tunning} em todos os treinamentos. Essa escolha foi feita porque essa base de dados
é muito ampla pela quantidade de classes que ela contém. Os pesos de 
suas camadas superficiais são gerais e funcionam para praticamente qualquer problema e com isso 
consegue-se iniciar o treinamento de uma etapa mais avançada, sem precisar aprender esses pesos das 
camadas mais superficiais \cite{DeCAF}.

Foi utilizada uma taxa de aprendizado inicial de $0.0002$, que é uma taxa de aprendizado
relativamente pequena. Para garantir
a convergência, foi utilizado um \textit{weight decay} (parâmetro que penaliza os pesos grandes) de 
$0.0005$ para dar uma pequena penalização nos maiores pesos, e uma rotina de diminuição da taxa de 
aprendizado, definida na Equação \ref{eq:rot}, a partir da época 100. Os treinamentos foram programados
para realizar 150 épocas, no entanto os pesos salvos no fim foram aqueles que alcançaram melhor resultado 
parcial no conjunto de validação. Cada época do treinamento foi dividida em vários \textit{batches}, onde 
cada \textit{batch} continha $64$ imagens, sendo $4$ imagens por pessoas e imagens de $16$ pessoas 
distintas por \textit{batch}.

\begin{equation}
    lr = lr \ (0.001)^{(\text{época} - 100) / 50}
    \label{eq:rot}
\end{equation}

\section{Métodos de adaptação de domínio}
\label{met_adap}

Para o desafio de re-identificação de pessoas, pode-se ver cada uma das bases de dados utilizadas 
como um domínio, pois como visto no capítulo \ref{CapBaseDeDados} cada base apresenta parâmetros 
únicos e distintos das outras, como ângulo, número de câmeras, iluminação, qualidade da imagem, 
distância das pessoas para a câmera. Portanto, é interessante analisar os resultados de uma rede 
neural treinada em uma base fonte nos dados de uma base alvo e tentar melhorar esses resultados 
utilizando técnicas de transferência de aprendizado.

A re-identificação de pessoas tem uma grande proximidade com o desafio de reconhecimento facial e 
ambas as técnicas só serão bem aceitas para aplicações reais genéricas quando se demonstrarem 
robustas quanto ao local de aplicação da tecnologia. Ou seja, não adianta ter a melhor tecnologia de 
re-identificação de pessoas se ela só funciona em seu laboratório ou na sua base de dados, pois 
o custo para adaptar essa tecnologia em um novo ambiente seria muito caro se toda a parte de 
aquisição e anotação de dados para gerar uma nova base de dados e de treinamento das redes neurais 
precisasse ser repetida.

Portanto, nessa seção vamos analisar vários métodos para adaptar uma rede neural treinada numa 
base de dados fonte de forma que ela performe melhor nos dados de uma base alvo. Analisaremos
tanto métodos supervisionados quanto métodos não supervisionados para ver a diferença entre eles 
e entender o quanto de trabalho se têm para se fazer essa adaptação de domínio.

\subsection{Método 1 - Transferência Direta \textit{(Direct transfer)}}
\label{direct_transfer_}

O método de transferência direta consiste em utilizar uma rede neural pré-treinada 
em um domínio fonte para analisar o seus resultados em um domínio alvo. Ou seja, utiliza-se as 
redes neurais treinadas na seção \ref{params} para avaliar os seus resultados em  outras 
bases de dados que não a utilizada para o treinamento.

Esse método assume que para os dois domínios a tarefa (geração de vetor de características) e os 
espaços de rótulos (pessoas) são iguais, 
portanto $\tau^f = \tau^a$ e $Y^f = Y^a$. Essa afirmativa é verdadeira, pois a rede neural 
treinada com a função de custo \textit{triplet}, como mencionado na seção \ref{params}, aprende uma 
métrica que aproxima imagens da mesma pessoa e distancia imagens de pessoas distintas. Logo, mesmo 
que as bases de dados tenham pessoas distintas o objetivo do algoritmo é o mesmo (aproximar/afastar imagens 
de pessoas). No entanto, por se 
tratar de  domínios diferentes, cada um com suas particularidades (iluminação, angulação, distância da 
câmera para as pessoas), espera-se que a maneira de gerar o vetor de características das imagens seja 
distinta $(P(Y|X^f) \neq P(Y|X^a))$. Por isso, não se espera um bom resultado para esse método.

Em nenhum momento de treinamento foram 
apresentados dados anotados da base alvo. Mesmo que o resultado esperado desse método não seja muito 
bom, ele é muito interessante, pois permite que seja feita uma análise de quão distantes são 
duas bases de dados. 

\subsection{Método 2 - \textit{Fine Tunning}}

Essa técnica é configurada quando 
se copia os pesos de uma rede neural treinada numa base dados fonte para iniciar o treinamento de uma 
rede neural em uma base de dados alvo. O nome \textit{fine tunning} é utilizado, pois acredita-se que o 
aprendizado geral já ocorreu e o treinamento com essa técnica representa apenas um ajuste fino do aprendizado.

Esse mesmo método foi utilizado nos treinamentos das redes neurais para avaliação no próprio domínio de 
treinamento, como descrito na seção \ref{params}. No entanto, no caso anterior utilizamos pesos de uma 
rede treinada na \textit{ImageNet} \cite{imagenet_cvpr09}, portanto a camada final de classificação da 
rede teve que ser retirada, pois trata-se de uma tarefa distinta. Para a situação atual não há necessidade
de retirar essa última camada por se tratar da mesma tarefa e do mesmo espaço de rótulos.

O objetivo do uso dessa técnica no treinamento anterior tinha sido de acelera-lo, seguindo a premissa que 
as camadas superficiais aprendem representações superficiais e genéricas, logo só seria necessário aprender 
as representações mais específicas do desafio em questão nas camadas mais profundas. Agora, acredita-se que 
esse método irá adaptar essas representações mais específicas da re-identificação de pessoas de um domínio 
fonte para um domínio alvo, portanto o objetivo desse método é aprender as particularidades de um novo 
domínio.

Espera-se que esse método apresente os melhores resultados dentre todos os métodos de adaptação de domínio que serão estudados 
nesse trabalho. Pois, ele terá todos os dados do domínio alvo devidamente anotados e partirá de um estágio 
avançado de treinamento, podendo, inclusive, ultrapassar os resultados adquiridos com o método da seção 
\ref{params}. Porém, esse é o método mais caro de adaptação de domínio, pois há a necessidade de obter uma
grande quantidade de dados anotados manualmente no domínio alvo para o novo treinamento.


\subsection{Método 3 - \textit{Pseudo} rótulos}

Esse método consiste em utilizar uma rede neural, treinada em uma base de dados fonte, para classificar os 
dados de uma base de dados alvo. Assume-se que essa classificação é perfeita e utiliza-a para 
anotar os dados da base de dados alvo, o treinamento é refeito utilizando os pseudo rótulos 
gerados pela anotação. No entanto, a re-identificação de pessoas não é um desafio de 
classificação, portanto surge o questionamento: Como utilizar a saída de uma rede neural de re-identificação 
para anotar os dados de outra base de dados?

Uma vez que essas redes neurais foram treinadas usando a função de custo \textit{triplet}, a saída da rede 
é um vetor de características que pertence a um espaço vetorial euclidiano (dado que a \textit{triplet} foi
treinada utilizando a distância euclidiana como função de comparação). Portanto, para anotar os dados de outra 
base a partir das saídas da rede neural, precisa-se agrupar os vetores de características de forma que cada 
grupamento represente uma pessoa.

O algoritmo de agrupamento escolhido foi o \textit{k-means} \cite{kmeans}. Para um espaço
vetorial contendo $N$ amostras (cada amostra representa o vetor de características de uma imagem da base de 
dados alvo), esse algoritmo consiste em inicializar $K$ \textit{clusters}, de forma aleatória, dentro do espaço 
vetorial e agrupar $n$ ($0 < n \leq N$) amostras para cada \textit{cluster}. O agrupamento é feito 
utilizando a política do vizinho mais próximo, ou seja, cada amostra pertencerá ao \textit{cluster} 
com a menor distancia euclidiana para ela (a Equação \ref{eq:kmeans} demonstra esse agrupamento).

\begin{equation}
    \begin{split}
        S_{k_i} = \{x_p : \left \|  x_p - k_i\right \|^2 < \left \|  x_p - k_j\right \|^2 \ \forall j, 1 \leq j \leq K, \ j \neq i, 1 \leq p \leq N\}, \\ \\
        \begin{aligned}
                          & S_{k_i} \text{é o conjunto de vetores de características associados ao \textit{cluster} } k_i \\
                          & x_p \text{ representa cada um dos vetores de características}
        \end{aligned}
    \end{split}
    \label{eq:kmeans}
\end{equation}


No entanto, um primeiro agrupamento não é ótimo, pois a posição dos $K$ \textit{clusters} iniciais é feita 
de forma aleatória. Logo, após um primeiro agrupamento, pode-se atualizar a posição dos $K$ \textit{clusters}
para a posição do centroide do grupo associado a ele, conforme demonstra a Equação \ref{eq:update}. Com essa 
nova posição dos $K$ \textit{clusters}, aplica-se novamente a Equação \ref{eq:kmeans} para gerar um novo 
agrupamento e esse processo se repete por um número pré-determinado de iterações ou até atingir convergência
(não mudar os agrupamentos após a atualização da posição dos \textit{clusters}). A Figura \ref{fig:kmeans}
ilustra o processo de agrupamento do algoritmo \textit{k-means}.

\begin{equation}
    \begin{split}
        \begin{aligned}
                          & \text{ \ \ \ \ \  \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ }
                          k_i = \frac{1}{| S_{k_i} |} \sum_{x_j \in S_{k_i}} x_j, \\ \\
                          & k_i \text{ é a nova posição do \textit{cluster} i}  \\
                          & | S_{k_i} | \textit{ é a quantidade de vetores pertencentes ao \text{cluster} } k_i
        \end{aligned}
    \end{split}
    \label{eq:update}
\end{equation}

\begin{figure}[htbp]
    \centering

    {\includegraphics[width=1.0\textwidth]{figs/k-means.png}}%
    
    \caption{Exemplo do funcionamento do algoritmo \textit{k-means}.
    (a) Inicialização inicial aleatória dos \textit{cluster}. (b) Primeiro agrupamento, utilizando a 
    inicialização aleatória. (c) Correção da posição dos \textit{clusters} utilizando os centroides dos grupos.
    (d) Novo agrupamento a partir da correção da posição dos \textit{clusters}. As etapas (c) e (d) são repetidas
    até atingir convergência ou um número máximo, pré-determinado de iterações. Reproduzida de \cite{kmeans_image}.}
    \label{fig:kmeans}
\end{figure}

Esse é um método de aprendizado não supervisionado, no entanto, percebeu-se que se ele fosse usado da maneira 
descrita a taxa de erro seria altíssima, não ajudando no resultado final. Portanto, após algumas observações 
foram criadas as seguintes restrições:

\begin{itemize}
    \item \textbf{K > Qtd. ids:} O número de \textit{clusters} $K$ define a quantidade de grupos que serão formados,
portanto esse número precisa ser maior que a quantidade de pessoas distintas na base de dados alvos. Pois, para
$K < Qtd. ids$ com certeza serão formados grupos com imagens de mais de uma pessoa, e para $K > Qtd. ids$ pode-se 
formar mais de um grupo com imagens de uma mesma pessoa que representa um problema menor para o aprendizado. A Tabela 
\ref{table:kmeans} mostra a relação da quantidade de pessoas distintas com a quantidade de grupos formados em cada 
base de dados estudada. Para a base de dados \textit{Viper}, tem-se $K = Qtd.\ ids$, pois essa base apresenta apenas 1
imagem de cada pessoa por câmera.
    \item \textbf{\textit{K-means} para cada câmera:} Percebeu-se que as imagens de duas pessoas distintas, mas vistas pela
mesma câmera apresentam vetores de características mais próximos que imagens da mesma pessoa vista em câmeras diferentes.
Portanto, os grupos formados estavam apresentando imagens apenas de pessoas na mesma câmera, então foi feito um agrupamento 
de \textit{k-means} para cada câmera e depois utilizou-se o algoritmo de vizinhos mais próximos para agrupar os grupos entre 
câmeras.
\end{itemize}

\begin{table}[]
    \centering
    \caption{Tabela de relação entre o número de pessoas distintas nas bases de dados e o $k$ utilizado 
    para o algoritmo \textit{k-means} nessa base de dados. \newline}

    \begin{tabular}{|c|c|c|}
    \hline
    \textbf{Base de dados alvo} & \textbf{Qtd. ids} & \textbf{K} \\ \hline
    \textbf{CUHK03}             & 1360              & 2000       \\ \hline
    \textbf{Market1501}         & 1501              & 1600       \\ \hline
    \textbf{Viper}              & 632               & 632        \\ \hline
    \textbf{CyberQueue}       & 6261              & 6500       \\ \hline
    \end{tabular}
    \label{table:kmeans}
\end{table}

Após esse processo de agrupamento dos vetores de características de uma base de dados alvo, espera-se que 
cada grupo represente uma pessoa única, portanto pode-se criar uma base de dados alvo modificada utilizando 
esses agrupamentos como \textit{pseudo} rótulos. A partir dessa base de dados alvo modificada é feito um 
treinamento e uma avaliação dos resultados na base de dados alvo original para analisar se houve uma ganho 
de performance ou não.

\subsection{Método 4 - Uso de GAN cíclica como pré-processamento de dados}

Esse método foi proposto por Deng et al.\ \cite{Deng_2018_CVPR} e consiste em treinar uma GAN \cite{GAN_Goodfellow}
para tentar aproximar um domínio fonte de um domínio alvo. Assim melhorar o resultado da rede neural no 
domínio alvo, mesmo ela sendo treinada no domínio fonte. No entanto, esse método apresenta a dificuldade 
de não existirem imagens pareadas entre duas bases de dados distintas, por isso surge a ideia de usar 
a GAN cíclica de Zhu et al.\ \cite{Unpaired_ImageToImage_CycleGAN}.

O primeiro passo desse método consiste em treinar uma GAN cíclica de forma que ela aprenda a mapear
imagens entre os domínios fonte e alvo. Nessa etapa, a GAN aprende uma função geradora $G$ que, dada 
uma imagem $x$ do domínio fonte com função de densidade de probabilidade (fdp) $p_{fonte}$, a 
transformação $G(x)$ dela vai apresentar uma fdp $p_g$ que aproxima a fdp do domínio alvo ($p_{alvo}$), logo para 
uma imagem qualquer $x \in p_{fonte}$ a transformação $G(x)$ gera $p_g \approx p_{alvo}$. Por propriedade, 
a GAN cíclica também irá aprender uma função $F$ que para $y \in p_{alvo}$ a transformação $F(y)$ gera 
$p_f \approx p_{fonte}$. A Figura \ref{fig:GAN_reid} ilustra o funcionamento das funções geradoras.


\begin{figure}[htbp]
    \centering

    {\includegraphics[width=1.0\textwidth]{figs/reid_gan.png}}%
    
    \caption{Exemplo do funcionamento das funções geradoras $F$ e $G$ aprendidas por uma GAN cíclica.
    Onde, $D_\tau = d_{alvo}$ (domínio alvo) e $D_s = D_{fonte}$ (domínio fonte). Reproduzida de \cite{Deng_2018_CVPR}. 
    \copyright 2018 IEEE.}
    \label{fig:GAN_reid}
\end{figure}

Uma vez que as funções geradoras aprendem a transformar as imagens de um domínio fonte de forma
que elas fiquem mais próximas de um domínio alvo, utiliza-se essa técnica como pré-processamento
dos dados antes do treinamento. Portanto, espera-se que esse treinamento gere uma rede neural que performe 
melhor no domínio alvo. A GAN cíclica é utilizada nesse caso por sua característica de facilitar o treinamento
sem imagens pareadas.

O treinamento das GANs para esse método seguiu as estruturas de redes propostas por Zhu et al.\ \cite{Unpaired_ImageToImage_CycleGAN},
utilizando a Equação \ref{eq:custo_cycle} com $\lambda = 10$ para $25$ épocas (com taxa de aprendizado inicial de $0.0002$ e \textit{weight decay}
a partir da época $15$). As imagens utilizadas para esse treinamento foram de $128 \times 256$ \textit{pixels}, para
manter o mesmo padrão utilizado no treinamento das redes de re-identificação. No conjunto de treinamento foram 
inseridas, no máximo, $1000$ images por vista de câmera, pois se mostrou mais interessante iterar sobre o conjunto 
de treinamento várias vezes, ao invés de iterar por mais imagens distintas.

\section{Problemas de convergência da \textit{triplet}}
\label{problema}

Os treinamentos iniciais foram feitos de forma supervisionada, utilizando dados de apenas uma base de dados. Eles foram 
feitos de forma bem pragmática, como foi determinado na seção \ref{params}. No entanto, para o restante dos treinamentos
não foi possível definir um padrão de parâmetros utilizados, pois como os métodos de adaptação alteram 
as bases de dados, essas não se comportam mais tão bem quanto a original. Isso pode gerar diversas dificuldades 
durante o treinamento. 

A maior dificuldade encontrada estava relacionada à convergência da função de custo \textit{triplet}. 
Para cada \textit{anchor} (imagem) do \textit{batch} de treinamento era 
escolhida como seu exemplo positivo outra imagem do \textit{batch} que pertencia a mesma pessoa. De forma que 
essa fosse a imagem que apresentasse a maior distância do \textit{anchor} (essa imagem representa o exemplo positivo mais difícil 
do \textit{batch}). Para o exemplo negativo era escolhida uma imagem de outra pessoa, que apresentasse a 
menor distância da \textit{anchor}, caracterizando-se como o exemplo negativo mais difícil do \textit{batch}.
Esse método de sempre selecionar os exemplos mais difíceis durante o treinamento se chama \textit{batch hard}.
Hermans et al.\ \cite{defense} realizaram um estudo sobre diversas maneiras de escolher os exemplos 
para acompanhar uma \textit{anchor}. O \textit{batch hard} apresentou os melhores resultados.

O grande problema do \textit{batch hard} é quando o treinamento chega em uma etapa onde a distância do 
\textit{anchor} para o exemplo negativo é aproximadamente igual a distância do mesmo para o exemplo positivo.
Nessa etapa, se os dados forem muito difíceis, a rede pode acabar aprendendo que se ela gerar o mesmo vetor de 
características para todas as entradas, a função de custo sempre será reduzida para o valor da margem, independente
da dificuldade apresentada pelo exemplo positivo/negativo. No entanto, chegar nesse ponto significa que todos os 
pesos da rede convergiram para o valor $0$. Contudo, ela não aprendeu nada e o treinamento fica preso nesse 
ponto de não aprendizado.

Para tentar resolver esse problema foram testadas várias técnicas distintas:

\begin{itemize}
    \item Reduzir muito a taxa de aprendizado para ver se um aprendizado mais lento poderia passar desse ponto sem 
    problemas, no entanto só demorava mais para chegar no mesmo ponto;
    \item Utilizar diferentes tipos de otimizadores, com isso foram observadas 
    diferenças na convergência durante o começo do treinamento, mas todos chegaram no mesmo ponto com mudanças 
    apenas no tempo;
    \item Usar taxas de aprendizado cíclicas \cite{cycle_LR} para 
    testar se uma rápida subida na taxa de aprendizado iria retirar a rede desse ponto de não aprendizado, mas 
    com o passar do ciclo seguinte ela voltava para o mesmo ponto;
    \item Acrescentar um ruído 
    aleatório a alguns pesos da rede de forma que eles não convergissem para zero, mas isso só fez com que o 
    treinamento oscilasse ao redor do ponto de não aprendizado sem que a convergência ocorresse.
\end{itemize}

Após testar todos os métodos citados acima, chegou-se a conclusão de que devido as alterações feitas nos 
dados pelos métodos de adaptação de domínio, a tarefa tinha ficado muito complicada para ser aprendida
utilizando \textit{batch hard} e um \textit{batch} grande (o valor padrão nos testes foi de $64$ imagens por
\textit{batch}). Portanto, o método encontrado para resolver esse problema foi o método de aprendizado por 
etapas \footnote{Método inspirado em discussão encontrada em \url{https://github.com/VisualComputingInstitute/triplet-reid/issues/4}}. 
Esse método consiste em reduzir o \textit{batch} no início do treinamento, pois em um universo de 
imagens reduzido a tarefa é mais simples de se aprender. Quando a tarefa simples é aprendida aumenta-se o 
\textit{batch} dificultando um pouco a tarefa e dando continuidade no aprendizado. Esse processo se repete
até alcançar a convergência no treinamento. 

O algoritmo a seguir, em pseudo código, representa uma implementação básica desse método.

\begin{algorithm}
    \caption{Algoritmo do aprendizado por etapas.}
  
    \begin{algorithmic}
      \State $batch\_size = 8$
      \State $loss\_margin = 0.5$
      \For  {$i=0$ to $num\_epochs$} 
      \State        $loss = treino(i, batch\_size)$ 
                    \If{$loss < 0.8 \times loss\_margin$}
      \State            $batch\_size = batch\_size + 8$
                    \EndIf
      \EndFor
    \end{algorithmic}
 \end{algorithm}
  
  

\section{Métricas utilizadas para avaliação}
\label{metricas}

Para avaliar os resultados obtidos, por cada rede neural treinada, foi utilizado sempre o mesmo procedimento,
calculando algumas métricas que permitem a comparação dos resultados. As métricas escolhidas na 
avaliação são muito usadas na literatura para o desafio de re-identificação de pessoas,
são elas: \textit{mean Average Precision} (mAP) e \textit{Cumulative Matching Characteristics} (CMC). 
Ambas as métricas são calculadas em cima do conjunto de testes.

\subsection{\textit{Top-k Predições}}

Para analisar os resultados obtidos não é necessário que a rede neural aponte sempre o resultado 
correto com a maior probabilidade. Às vezes se o resultado correto está com a terceira maior probabilidade 
ele ainda pode ser útil. Portanto, para casos como esses, são utilizadas as \textit{top-k} predições, 
onde $k$ representa até  qual posição a predição para a resposta pode ser considerada
correta. 

Tomando o caso de re-identificação de pessoas como exemplo, pode-se imaginar um 
conjunto de 300 pessoas onde deseja-se saber a qual das pessoas uma certa imagem pertence. Portanto, analisa-se
essa imagem na rede neural, obtendo uma lista das pessoas mais prováveis são (onde a pessoa correta é a que 
está em negrito):

\begin{enumerate}
    \item Pessoa 295 -> 21\% de probabilidade;
    \item Pessoa 072 -> 19.5\% de probabilidade;
    \item \textbf{Pessoa 198} -> 18\% de probabilidade;
    \item Pessoa 210 -> 15.2\% de probabilidade;
    \item Pessoa 004 -> 12.3\% de probabilidade;
    \item Restante das pessoas somadas -> 14\% de probabilidade.
\end{enumerate}

Para esse caso, em específico, se for adotado um $k < 3$, tem-se que o resultado da rede está incorreto.
No entanto, para qualquer $k \geq 3$ esse resultado seria considerado correto. 

\subsection{\textit{Mean Average Precision} (mAP)}

A métrica mAP é dada pelo valor médio das métricas AP (\textit{Average Precision}) por classe do 
conjunto de teste. No caso desse trabalho cada pessoa representa uma classe no conjunto de testes.
Portanto, precisa-se entender como é feito o cálculo da AP para entender o que é a mAP.

Para entender como funciona o cálculo da AP é preciso conhecer os conceitos de precisão e \textit{recall}
(re chamada). A precisão é o percentual das suas predições que estão corretas, já a \textit{recall} 
calcula o quão boa é a rede para encontrar os exemplos corretos, por exemplo, se a rede consegue 
encontrar 75\% dos exemplos corretos em uma abordagem de \textit{top-5} predições, a \textit{recall} vai ter
um valor de 0.75.

Para cada predição, pode-se obter quatro resultados diferentes, eles são:

\begin{itemize}
    \item \textbf{Falso positivo (FP):} Quando a rede neural diz que aquele exemplo é verdadeiro, mas
é um exemplo negativo.
    \item \textbf{Falso negativo (FN):} Quando a rede neural diz que aquele exemplo é negativo, mas
é um exemplo verdadeiro.
    \item \textbf{Verdadeiro positivo (VP):} Quando a rede neural diz que aquele exemplo é verdadeiro e
é, realmente, um exemplo verdadeiro.
    \item \textbf{Verdadeiro negativo (VN):} Quando a rede neural diz que aquele exemplo é negativo e
é, realmente, um exemplo negativo.
\end{itemize}

Portanto, utilizando esses quatro resultados diferentes que podem ser obtidos em uma predição de um 
classificador, pode-se definir a precisão como indicado na Equação \ref{eq:precision} e a re chamada como indicado
na Equação \ref{eq:recall}.

\begin{equation}
    \text{Precisão} = \frac{VP}{VP + FP}
    \label{eq:precision}
\end{equation}

\begin{equation}
    \text{Recall} = \frac{VP}{VP + FN}
    \label{eq:recall}
\end{equation}

A Tabela \ref{table_precision} exemplifica como é feito o cálculo da precisão e da \textit{recall} para um 
caso onde há 3 exemplos positivos que devem ser encontrados e está sendo utilizada uma abordagem de 
\textit{top-5} predições. A primeira coluna da Tabela indica qual o \textit{rank} da predição, a segunda
coluna indica se aquela predição foi correta ou não, já a terceira e quarta colunas representam os 
valores da precisão e \textit{recall} naquele momento.

\begin{table}[htbp]
    \centering
    \caption{Tabela exemplo para ilustrar o cálculo da precisão e \textit{recall}.}
    \begin{tabular}{cccc}
    Rank & Correto? & Precisão & \textit{recall} \\
    1    & Sim      & 1.0      & 0.33      \\
    2    & Sim      & 1.0      & 0.67      \\
    3    & Não      & 0.67     & 0.67      \\
    4    & Não      & 0.5      & 0.67      \\
    5    & Sim      & 0.6      & 1.0      
    \end{tabular}
    \label{table_precision}
\end{table}

Se for plotado um gráfico de \textit{recall} $\times$ precisão, conceitualmente, o valor da AP pode ser encontrado
calculando a área abaixo desse gráfico. No entanto, o cálculo mais utilizado da AP é feito discretizando
a \textit{recall} em intervalos de 0.1, calculando a maior precisão para cada intervalo (como apresentado na 
Equação \ref{eq:max_pre}) e fazendo uma média aritmética do valor da precisão em todos os intervalos
(Equação \ref{eq:AP}).

\begin{equation}
    AP_r (r) = \max(\text{precisão}) \ \forall r^{'} > r
    \label{eq:max_pre}
\end{equation}

\begin{equation}
    AP = \frac{1}{11} \sum_{n = 0}^{1.0} AP_r(n) \\
    AP = \frac{1}{11} (AP_r(0) + AP_r(0.1) + ... + AP_r(0.9) AP_r(1.0))
    \label{eq:AP}
\end{equation}

Por fim, a métrica mAP é calculada fazendo-se a média aritmética da AP para cada classe do conjunto de 
dados. Portanto, para um conjunto de dados com $C$ classes ($C$ pessoas distintas), a mAP pode ser 
calculada como na Equação \ref{eq:MAP}.

\begin{equation}
    MAP = \frac{1}{C} \sum_{n = 0}^{C} AP_n
    \label{eq:MAP}
\end{equation}

\subsection{\textit{Cumulative Matching Characteristics} (CMC)}

A CMC é a métrica mais importante para ser analisada nesse trabalho, pois essa métrica é a mais popular 
entre as métricas utilizadas para avaliar os métodos de re-identificação de pessoas. 

Para um conjunto de dados de teste com $N$ imagens, cada imagem de teste será comparada com todas as outras 
$N-1$ imagens existentes no conjunto e ordenadas por quais são as mais prováveis de pertencerem a mesma pessoa
até as menos prováveis. Caso tenha uma imagem da mesma pessoa nas \textit{top-k} predições, considera-se que 
o resultado foi correto (positivo) e atribui-se $1$ ao acumulador parcial ($A_{n}$) da CMC, caso contrário atribui-se $0$
(Equação \ref{eq:individual}). Após aplicar esse procedimento para todas as $N$ imagens do conjunto, soma-se
os valores de todos os acumuladores parciais para se obter o acumulador total $A_{cc}$ (\ref{eq:total}) e uma média aritmética
é aplicada ao acumulador total para calcular o índice CMC (Equação \ref{eq:CMC}).

\begin{equation}
    A_n = \left\{\begin{array}{l}
        1, \text{ se   o   resultado  esperado  estiver  nas  \textit{top-k}  predições}\\ 
        0, \text{ caso contrário}
        \end{array}\right.
    \label{eq:individual}
\end{equation}

\begin{equation}
    A_{cc} = \sum_{n = 1}^{N} A_n
    \label{eq:total}
\end{equation}

\begin{equation}
    CMC = \frac{A_{cc}}{N}
    \label{eq:CMC}
\end{equation}

A Figura \ref{fig:MAPxCMC} mostra um exemplo da diferença entre as métricas CMC e AP. Nesta Figura,
tem-se que os quadrados verdes representam as comparações positivas e os quadrados vermelhos representam
as comparações negativas, e no exemplo (a) havia apenas uma comparação positiva para ser encontrada
enquanto nos exemplos (b) e (c) haviam duas comparações positivas para serem encontradas em cada.


\begin{figure}[htbp]
    \centering

    {\includegraphics[width=0.7\textwidth]{figs/MAPxCMC.png}}
    
    \caption{Comparação entre as métricas AP e CMC. Para esses três exemplos e utilizando 
    uma abordagem de \textit{top-5} predições, tem-se que a métrica CMC vale 1 para todos,
    enquanto a métrica AP tem resultados que variados. Reproduzida de \cite{zheng2015scalable}. \copyright 2015 IEEE.}
    \label{fig:MAPxCMC}
\end{figure}

\subsection{Tipos de comparações}

O conjunto de testes contém sempre um número $N$ de pessoas diferentes.
Para cada pessoa há um número $M$ de vistas de câmeras distintas, ou seja, cada uma das $N$ pessoas 
pode ter imagens de $1$ até $M$ vistas de câmeras distintas. Esse tipo de organização
pode gerar uma confusão sobre como devemos selecionar as imagens para fazer a comparação, logo alguns 
autores já fizeram comparações diferentes. Portanto, nesse trabalho os testes utilizaram  
três abordagens diferentes quanto às comparações.

\textbf{Abordagem \textit{Allshots}:} Para cada imagem do conjunto de testes, faz-se uma comparação dela
com um conjunto contendo uma imagem aleatória de cada pessoa de cada vista, excluindo apenas imagens 
da pessoa e vista em análise. 
No geral, é a abordagem que relata os piores resultados,
pois é a que seleciona um volume maior de imagens para comparação.

\textbf{Abordagem \textit{CUHK03}:} Para cada imagem do conjunto de testes, faz-se uma comparação dela
com um conjunto contendo uma imagem de cada pessoa e de uma vista diferente. Por 
limitar bastante o universo de comparação, aumenta as chances do resultado ser positivo e, portanto, 
é a abordagem que relata os valores mais altos.

\textbf{Abordagem \textit{Market1501}:}  Cada imagem do conjunto de testes é comparada
com o restante do conjunto excluindo as imagens que são da mesma
pessoa e da mesma vista. Esse método é um pouco mais difícil que o \textit{CUHK03}, pois 
ele tem um universo de comparação maior, incluindo todas as vistas. No entanto, ele é mais 
fácil que o \textit{Allshots}, pois inclui todas as imagens da mesma pessoa em outras vistas, 
o que garante que os exemplos fáceis sempre estarão presentes no universo de comparação.

A Figura \ref{fig:comparation} ilustra cada um dos tipos de comparação utilizados para facilitar 
o entendimento desses.

\begin{figure}[htbp]
    \centering

    {\includegraphics[width=1.0\textwidth]{figs/comparation.png}}%
    
    \caption{Exemplos dos tipos de comparação utilizados. Onde, A, B e C representam pessoas distintas,
    cada pessoa tem imagens em duas vistas e cada divisão do retângulo representa uma imagem. A divisão
    com um ponto vermelho representa a imagem a ser comparada e as divisões com pontos em verde representam 
    as imagens que fazem parte do universo de comparação.}
    \label{fig:comparation}
\end{figure}
